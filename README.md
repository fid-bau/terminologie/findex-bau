# FINDEX Bau

## PURL und Versionsinformation

* PURL: <https://terminology.fraunhofer.de/voc/bau>
* Version: 0.0.6
* Version IRI: <https://gitlab.com/fid-bau/terminologie/findex-bau/-/raw/0.0.6/findex-bau.ttl>
* [Alle Releases](https://gitlab.com/fid-bau/terminologie/findex-bau/-/releases)

## Lizenz

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz.

## Vorschläge und Beteiligung

Bitte beachten Sie hierzu unsere [Hinweise zur Beteiligung](/CONTRIBUTING.md).

## Über den FINDEX Bau

Die SKOS-Version des FINDEX Bau wurde vom Fraunhofer-Informationszentrum Raum und Bau IRB im Kontext des gemeinsamen Verbund-Projekts FID BAUdigital in Kooperation mit der TIB - Technische Informationsbibliothek erstellt. Sie basiert lose auf der 2. Auflage des FINDEX Bau, die 1985 durch den IRB-Verlag veröffentlicht wurde.

Die Print-Ausgabe des FINDEX Bau wurde erstmalig 1982 veröffentlicht und bis zur 2. Auflage mit Hilfe von Anmerkungen der am IRB tätigen Bauingenieure und Architekten sowie von Nutzern der IRB-Datenbanken und den für das IRB tätigen Erschließenden überarbeitet und erweitert.

Der FINDEX Bau ist ein facettenartiges Indexierungssystem für das Bauwesen und dient dem Ordnen und Wiederauffinden von Informationen. Seine Grundstruktur wurde in Zusammenarbeit mit dem Arbeitsausschuss I 21 \"Begriffe im Bauwesen\" des Normenausschusses Bauwesen (NABau) im Deutschen Institut für Normung (DIN) erarbeitet und wurde auch innerhalb des Internationalen Rats für Bauordnung und Baudokumentation (CIB) abgestimmt.

Der FINDEX Bau wurde zur Schrifttumsdokumentation entwickelt und erhebt aus diesem Grund keinen Anspruch auf Vollständigkeit. Er wurde zur Indexierung der Informationen der deutschsprachigen Literaturdatenbanken RSWB (= Raumordnung, Städtebau, Wohnungswesen, Bauwesen) und LINA (= Literaturnachweise), der Forschungsprojektdatenbank BAUFO (= Bauforschungsprojekte) und der Bauobjektdatenbank BUDO (= Bauobjektdokumentation) erstellt und zum Zweck der Indexierung von Informationen der englischsprachigen Literaturdatenbank ICONDA (= International Construction Database) ins Englische übersetzt.

In der Print-Version des FINDEX Bau wurden einige formale Regeln befolgt, z.B., dass Komposita zulässige Begriffe darstellen, dass sich die Schreibung am Duden orientiert, Begriffe üblicherweise im Singular angegeben werden u.a.

Der FINDEX Bau enthält kein hierarchisches Begriffssystem mit Abstraktionsbeziehungen, ist jedoch trotzdem in vier Ebenen gegliedert, wobei die erste Ebene 20 Facetten enthält, die auf den nachfolgenden Ebenen zwei und drei in weitere Facetten untergliedert werden. Auf Ebene 1 finden sich folgende Hauptbegriffe:

* Bauart
* Baudurchführung
* Bauliche Anlagen
* Baunormung
* Baunutzung
* Bauplanung
* Baustoff und Halbzeug
* Bauteil
* Bauwirtschaft
* Beteiligte
* Fachbereiche
* Faktoren der physischen Umwelt
* Faktoren der sozialen Umwelt
* Herstellung
* Maschine und Gerät
* Methode
* Raum und Geometrie
* Recht
* Technischer Ausbau
* Tragwerk

Die SKOS-Version versucht die ursprüngliche Struktur des FINDEX Bau so genau wie möglich mit den durch den SKOS-Standard (vgl. <https://www.w3.org/TR/skos-reference/>) bereitgestellten Mitteln abzubilden.

Im Rahmen des FID BAUdigital soll die SKOS-Version des FINDEX Bau als ein Modul neben weiteren Thesaurus-Modulen für den Einsatz in den Informationsportalen des FID BAUdigital eingesetzt werden. Seine inhaltliche Weiterentwicklung ist hierbei nicht geplant. Stattdessen soll ihm ein weiteres Modul zur Seite gestellt werden, das Begriffe des digitalen Planens und Bauens beinhalten wird.

## Zitation

Fraunhofer-Informationszentrum Raum und Bau IRB (Hrsg.): FINDEX Bau als SKOS-Thesaurus. Facettenartiges Indexierungssystem für das Bauwesen, 2021. URL: <https://terminology.fraunhofer.de/voc/bau>.

## Referenzen

* Fraunhofer-Informationszentrum Raum und Bau IRB (Hrsg.): FINDEX Bau. Facettenartiges Indexierungssystem für das Bauwesen. Eine systematische Begriffssammlung zum Ordnen und Suchen von Bauinformationen. Systematischer und alphabetischer Teil. 2. Aufl., Stuttgart: Fraunhofer IRB Verlag, 1985. Eintrag im Katalog der Deutschen Nationalbibliothek: <http://d-nb.info/860410269>.
* Fraunhofer-Informationszentrum Raum und Bau IRB (Hrsg.): Facet-oriented Indexing System for Architecture and Construction Engineering. A systematic Collection of Terms for Classification and Searching of Building Informations. Systematic and Alphabetic Part, 1. Aufl., Stuttgart: Fraunhofer IRB Verlag, 1985. Eintrag im Katalog der Deutschen Nationalbibliothek: <http://d-nb.info/860068544>.

## Externe Links

* [FINDEX Bau auf dem TIB Terminology Service](https://terminology.tib.eu/ts/ontologies/bau)

## Schlagwörter

* [Bauwesen](http://d-nb.info/gnd/4144204-0)
* [Bauwesen](http://uri.gbv.de/terminology/bk/56)
* [Architektur, Bautechnik](https://d-nb.info/standards/vocab/gnd/gnd-sc#31.3)
