# Erstellen einer TS-Distribution des Findex Bau

Im Ordner TS liegt eine Kopie des Findex Bau, die für die Einbindung des Findex Bau in den Terminology Service der Technischen Informationsbibliothek verwendet wird. Diese Kopie ist inhaltlich identisch mit der SKOS-Version, wurde jedoch durch Statements und Punning angereichert.

1. Ersetze [TS\findex-bau.owl](TS\findex-bau.owl) durch [findex-bau.owl](findex-bau.owl).

2. Öffne [TS\findex-bau.owl](TS\findex-bau.owl) mit Protege.

3. Aktualisiere die versionIRI der TS-Version nach Muster ```https:\/\/gitlab\.com\/fid-bau\/terminologie\/findex-bau\/-\/raw\/\d.\d.\d\/TS\/findex-bau\.owl```

4. Importiere <https://purl.org/fidbaudigital/subjects#P49dfc9fd-8edc-46f8-ac84-f58a207cb9fe>.

5. Führe folgenden Query aus:

    ```SPARQL

    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    prefix skos: <http://www.w3.org/2004/02/skos/core#>


    CONSTRUCT {
        ?s a owl:Class . 
        ?s <https://purl.org/fidbaudigital/subjects#P49dfc9fd-8edc-46f8-ac84-f58a207cb9fe> ?notation .
        }

    WHERE {
        ?s a skos:Concept .
        ?s skos:notation ?notation .
        }
    ```

6. Im Anschluss müssen die hierarchischen Beziehungen erstellt werden:

    ```SPARQL
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    
    CONSTRUCT { ?s a owl:Class . ?s rdfs:subClassOf ?y . } 
    WHERE { ?s a skos:Concept . ?s skos:broader ?y.}

    ```

7. Speichern.
