# OpenAlex2bau

This directory contains mappings from OpenAlex concepts to FINDEX Raum concepts. They are derived from [/mappings/bau2OpenAlex/bau2OpenAlex%2Bmetadata.ttl](/mappings/bau2OpenAlex/bau2OpenAlex%2Bmetadata.ttl). In particular, the first version of this mapping set was created with <https://gitlab.com/fid-bau/terminologie/findex-bau/-/raw/ecc7444c6d2f68b9c3ad988692eb56b6dc9ee7c7/mappings/bau2OpenAlex/bau2OpenAlex+metadata.ttl>. They are intended to be used in [FID BAUdigital Forschungsatlas](https://forschungsatlas.fid-bau.de/).

The mapping set is available via this PURL: <https://purl.org/fidbaudigital/OpenAlex2bau/fa>.

It contains 897 exact matches (skos:exactMatch). In some cases, a concept is subject or object in several mapping statements.

## Procedure

|#|step|outputs|
|-|-|-|
|1|Import [/mappings/bau2OpenAlex/bau2OpenAlex%2Bmetadata.ttl](/mappings/bau2OpenAlex/bau2OpenAlex%2Bmetadata.ttl) with OpenRefine.<br>Import format: RDF/Turtle, character encoding UTF-8.|--|
|2|process imported file with [/mappings/OpenAlex2bau/procedure.json](/mappings/OpenAlex2bau/procedure.json)|--|
|3|Export project as RDF Turtle.|[/mappings/OpenAlex2bau/OpenRefine_export.ttl](/mappings/OpenAlex2bau/OpenRefine_export.ttl)|
|4|Open [/mappings/OpenAlex2bau/OpenRefine_export.ttl](/mappings/OpenAlex2bau/OpenRefine_export.ttl) with Protege or other ontology editor and save files in Turtle syntax (.ttl) and RDF/XML synatx (.owl).<br>This step gets rid of redundant occurrences of statements and renders the statements in a more readable format.|undocumented product|
|5|Add versionIRI to each output of step 4.|[/mappings/OpenAlex2bau/OpenAlex2bau.owl](/mappings/OpenAlex2bau/OpenAlex2bau.owl)<br>[/mappings/OpenAlex2bau/OpenAlex2bau.ttl](/mappings/OpenAlex2bau/OpenAlex2bau.ttl)|