# Mappings

This directory contains mappings from Findex Bau to other vocabularies.

|Mapping set|description|SSSOM|PURL to latest|
|-|-|-|-|
|[bau2raum](/mappings/bau2raum/)|Mappings to Findex Raum|false|<https://www.purl.org/fidbaudigital/bau2raum>|
|[bau2gnd](/mappings/bau2gnd/)|Mappings to GND (tbd)|tbd|tbd|
|[bau2OpenAlex](/mappings/bau2OpenAlex/)|Mapping to concepts on OpenAlex|true|https://www.purl.org/fidbaudigital/bau2OpenAlex|
|[OpenAlex2bau](/mappings/OpenAlex2bau/)|Mappings between Findex Bau and OpenAlex, derived from [/mappings/bau2OpenAlex/bau2OpenAlex%2Bmetadata.ttl](/mappings/bau2OpenAlex/bau2OpenAlex%2Bmetadata.ttl). Not employing full SSSOM schema. Intended for [FID BAUdigital Forschungsatlas](https://forschungsatlas.fid-bau.de/).|false|<https://purl.org/fidbaudigital/OpenAlex2bau/fa>|
<!-- ||||| -->
<!-- ||||| -->