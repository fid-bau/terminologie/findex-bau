# bau2raum

FINDEX Raum and FINDEX Bau have quite some overlap in concepts, therefore they were mapped with each other. The mappings from FINDEX Bau to FINDEX Raum can be found here.

These mappings have been automatically created with Open Refine rdf reconciliation and alignment maker. Mappings are mainly based on string similarity and therefore prone to error. The mappings have been roughly validated but are in a draft status.

If you find any errors or do not agree, we would like to encourage you, to [get in touch with us](https://gitlab.com/fid-bau/terminologie/findex-bau/-/issues).

You find the following mapping files:
|File|comment|
|-|-|
|[/mappings/bau2raum/bau2raum.owl](/mappings/bau2raum/bau2raum.owl)|This file contains mappings from FINDEX Bau to FINDEX Raum, applying skos:mappingRelation types. This is the master file for mappings between FINDEX Bau and FINDEX Raum.|
|[/mappings/bau2raum/bau2raum_exactMatch_DbXRef.owl](/mappings/bau2raum/bau2raum_exactMatch_DbXRef.owl)|This contains mappings from FINDEX Bau to FINDEX Raum, applying the mapping property <http://www.geneontology.org/formats/oboInOwl#hasDbXref>. This file will be used on the TIB Terminology Service. It has been created from [/mappings/bau2raum/bau2raum.owl](/mappings/bau2raum/bau2raum.owl) applying a SPARQL CONSTRUCT query. Only `skos:exactMatches` are present in this file.|