from rdflib import Graph
from pathlib import Path

FindexBauowl = Path(__file__).parent.parent.parent/'findex-bau.owl'
FindexBauttl = Path(__file__).parent.parent.parent/'findex-bau.ttl'
bau4TS = Path(__file__).parent.parent.parent/'TS'/'findex-bau.owl'
bau4TSSKOS = Path(__file__).parent.parent.parent/'TS'/'findex-bau-skos.owl'
bau2OpenAlex = Path(__file__).parent.parent.parent/ 'mappings' /'bau2OpenAlex'/ 'bau2OpenAlex+metadata.ttl'
OpenAlex2bauowl = Path(__file__).parent.parent.parent/ 'mappings' /'OpenAlex2bau'/ 'OpenAlex2bau.owl'
OpenAlex2bauttl = Path(__file__).parent.parent.parent/ 'mappings' /'OpenAlex2bau'/ 'OpenAlex2bau.ttl'
# print (FindexBauowl)
# print (FindexBauttl)
# print (bau4TS)
# print (bau2OpenAlex)
# print (OpenAlex2bauowl)
# print (OpenAlex2bauttl)


# def load_file(filepath, file_format):
#     g = rdflib.Graph()
#     g.parse(filepath, format=file_format)
#     return g

def parse_onto(onto_purl, onto_format):
    g = Graph()
    g.parse(onto_purl, format=onto_format)
    print(f"Graph {onto_purl} has {len(g)} statements.")
    assert len(g) > 0, f'Error: No triples found in {onto_purl}.'
    assert g, f'Error: {onto_purl} is not a graph'

parse_onto(str(FindexBauowl), onto_format="xml")
parse_onto(str(FindexBauttl), onto_format="turtle")
parse_onto(str(bau4TS), onto_format="xml")
parse_onto(str(bau2OpenAlex), onto_format="turtle")
parse_onto(str(OpenAlex2bauowl), onto_format="xml")
parse_onto(str(OpenAlex2bauttl), onto_format="turtle")
parse_onto(str(bau4TSSKOS), onto_format="xml")
